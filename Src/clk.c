/*
 * clk.c
 *
 *  Created on: 28 февр. 2021 г.
 *      Author: apshe
 */

#include "clk.h"

uint8_t rcc_init(void)
{
	RCC->CR |= RCC_CR_HSEON;								// start HSE generator
	for (uint32_t StartUpCounter=0; ; StartUpCounter++)
	{
		if (RCC->CR & RCC_CR_HSERDY) break;					// break if HSE is ON
		if (StartUpCounter > 0x1000U)
		{
			RCC->CR &= ~RCC_CR_HSEON;						// stop HSE if time is gone
			return RETVAL_ERR;
		}
	}

	RCC->PLLCFGR = 0;										// reset PLLCFGR register
	RCC->PLLCFGR |= (8		<< RCC_PLLCFGR_PLLM_Pos);		// PLLM = 8
	RCC->PLLCFGR |= (432	<< RCC_PLLCFGR_PLLN_Pos);		// PLLN = 432
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLP_0|RCC_PLLCFGR_PLLP_1;	// PLLP = 8
	RCC->PLLCFGR |= (9		<< RCC_PLLCFGR_PLLQ_Pos );		// PLLQ = 9
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSE;					// PLL source is HSE

	RCC->CR |= RCC_CR_PLLON;								// start PLL
	for (uint32_t StartUpCounter=0; ; StartUpCounter++)
	{
		if (RCC->CR & RCC_CR_PLLRDY) break;					// break if PLL is ON
		if (StartUpCounter > 0x1000U)
		{
			RCC->CR &= ~RCC_CR_HSEON;						// stop HSE if time is gone
			RCC->CR &= ~RCC_CR_PLLON;						// stop PLL if time is gone
			return 2;
		}
	}

	FLASH->ACR |= FLASH_ACR_LATENCY_1WS;					// wait states = 1 according table 7 in RM

	RCC->CFGR |= RCC_CFGR_PPRE1_2;							// APB1 prescaller = 2
	RCC->CFGR |= 0;											// APB2 prescaller = 1

	RCC->CFGR |= RCC_CFGR_SW_1;								// set PLL as system clock
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);	// wait turning PLL as system clock

	SystemCoreClockUpdate();

	return RETVAL_OK;
}

